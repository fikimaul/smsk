##
Sistem Informasi Surat
##
Mencatat Surat Masuk dan Surat Keluar dengan konsep Single Page Application

*******************
Features
*******************
- Menggunakan Framework CodeIgniter
- Single Page
- Sweet Alert
- Sb Admin Templates
- Datatables
- Session Login and Logout
- DatePicker

**************************
Next New Features
**************************
- JQuery Validation
- dll 


*******************
Developer
*******************
Fiki Maulana @fikimaul
