  <div class="row">
    <div class="col-lg-3">
      <h4 class="page-header">Surat Masuk</h4>
    </div>
    <div calass="col-lg-5" style="margin-top: 25px">
      <button class="btn btn-primary" data-toggle="modal" data-target="#tambahData"><span class="fa fa-plus-square"></span>   Tambah</button>
    </div>
  </div>
  <br/>
  <div class="row">
    <table width="100%" class="table table-striped table-bordered table-hover" id="surat">
      <thead>
      <tr class="success">
        <th>No</th>
        <th>Nomor Surat</th>
        <th>Dari</th>
        <th>Tgl Surat</th>
        <th width="20%">Aksi</th>
      </tr>
    </thead>
    </table>
  </div>

  <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Data Surat Masuk</h4>
      </div>
      <div class="modal-body">
        <form action="" method="post" accept-charset="utf-8" name="tambahSurat" id="formTambahSuratMasuk">
          <div class="form-group">
              <label>Nomor Surat</label>
              <input type="text" class="form-control" name="no_surat" required>
          </div>
          <div class="form-group">
              <label>Surat Dari</label>
              <input type="text" name="surat_dari" class="form-control" required>
          </div>
          <div class="form-group">
              <label>Tanggal Surat</label>
                <div class='input-group date' id='input_tanggal'>
                  <input type='text' class="form-control" name="tanggal_surat" required/>
                  <span class="input-group-addon">
                 		<span class="glyphicon glyphicon-calendar"></span>
                 	</span>
                </div>
          </div>
          <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input name="simpan" type="button" class="btn btn-success" value="Tambah" onclick="return simpanMasuk()">
        </form>
      </div>
    </div>
  </div>
  </div>

  <div class="modal fade" id="tampil_detail_sm" tabindex="-1" role="dialog" aria-labelledby="detail_sm" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Surat Masuk</h4>
      </div>
      <div class="modal-body">
        <table width="100%">
          <tr>
            <td>Id Surat</td>
            <td>: </td>
            <td><span id='data_id_surat'></td>
          </tr>
          <tr>
            <td>Nomor Surat</td>
            <td>: </td>
            <td><span id='data_no_surat'> </td>
          </tr>
          <tr>
            <td>Dari </td>
            <td>: </td>
            <td> <span id='data_dari'> </span></td>
          </tr>
          <tr>
            <td>Tanggal Surat </td>
            <td>: </td>
            <td><span id='data_tanggal'> </span> </td>
          </tr>
          <tr>
            <td>Keterangan </td>
            <td>: </td>
            <td><span id='data_keterangan'> </span> </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  </div>

  <div class="modal fade" id="edit_data_surat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Surat Masuk</h4>
      </div>
      <div class="modal-body">
        <form action="" method="post" accept-charset="utf-8" name="editSurat" id="formEditSuratMasuk">
          <div class="form-group">
              <label>ID Surat</label>
              <input type="text" class="form-control" name="edit_id_surat" id="edit_id_surat"  readonly required>
          </div>
          <div class="form-group">
              <label>Nomor Surat</label>
              <input type="text" class="form-control" name="edit_no_surat" id="edit_no_surat" required>
          </div>
          <div class="form-group">
              <label>Surat Dari</label>
              <input type="text" name="edit_surat_dari" id="edit_surat_dari" class="form-control" required>
          </div>
          <div class="form-group">
              <label>Tanggal Surat</label>
              <div class='input-group date' id='input_tanggal'>
                  <input type='text' class="form-control" name="edit_tanggal_surat" id="edit_tanggal_surat"  required/>
                  <span class="input-group-addon">
                 		<span class="glyphicon glyphicon-calendar"></span>
                 	</span>
                </div>
          </div>
          <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="edit_keterangan" id="edit_keterangan" class="form-control" required>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input name="simpan" type="button" class="btn btn-success" value="Simpan" onclick="return simpanEditMasuk()">
        </form>
      </div>
    </div>
  </div>
  </div>

</div>
<script>
$(document).ready(function() {
  var t = $('#surat').DataTable( {
      "ajax": "<?php echo site_url('/SuratMasuk/ambilData')?>",
      "columns": [
          { "data": null },
          { "data": "no_surat_masuk" },
          { "data": "dari" },
          { "data": "tanggal_surat_masuk" },
          {
            mRender: function (data, type, row) {
                 return '<button class="btn btn-primary btn-circle" onclick="viewDetail(' + row.id_surat_masuk + ')"><i class="glyphicon glyphicon-eye-open"></i></button> <button class="btn btn-success btn-circle" onclick="openEdit(' + row.id_surat_masuk + ')"><i class="fa fa-pencil-square-o"></i></button> <button class="btn btn-danger btn-circle" onclick="hapusSurat(' + row.id_surat_masuk + ')"><i class="glyphicon glyphicon-trash"></i></button>'
             }
          }
      ]
  } );

  t.on( 'order.dt search.dt', function () {
     t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
         cell.innerHTML = i+1;
     } );
  } ).draw();
});

function simpanMasuk(){
   var data = $("#formTambahSuratMasuk").serialize();

   $.ajax({
      data: data,
      type: "post",
      dataType:"json",
      url: "<?php echo site_url("/SuratMasuk/simpanSuratMasuk")?>",
      success: function(response){
         if(response.status=="gagal"){
         swal({
            title: "Gagal",
            text: "Gagal Menyimpan Data Surat Masuk",
            type: "error"
         });
         }
         else  {
            swal({
               title: "Berhasil",
               text: "Data Surat Masuk Berhasil Ditambahkan",
               type: "success"
            });
            $('#surat').DataTable().ajax.reload();
            $('#tambahData').modal('hide');
            document.getElementById("formTambahSuratMasuk").reset(); 
         }
      }
   });

}

function hapusSurat(id) {

  swal({
    title: "Apakah Anda Yakin Akan Menghapus Surat dengan id : "+id+" ?",
    text: "Dataakan dihapus permanen",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Ya",
    cancelButtonText: "Batal",
    closeOnConfirm: false,
    closeOnCancel: false
  },
      function(isConfirm){
        if (isConfirm) {
          $.ajax({
            data: {'id_surat':id},
            type: "post",
            url: "<?php echo site_url("/SuratMasuk/hapusSuratMasuk")?>",
            success: function(){
              swal({
                      title: "Berhasil",
                      text: "Data Surat Masuk Berhasil Dihapus",
                      type: "success"
                });
              $('#surat').DataTable().ajax.reload();

            }
          });
        } else {
          swal("Batal", "Perintah Dibatalkan", "error");
        }
  });
}

function viewDetail(id){
  $('#tampil_detail_sm').modal('show');
  $.ajax({
    data: {'id_surat':id},
    dataType:"json",
    type: "post",
    url: "<?php echo site_url("/SuratMasuk/ambilDetailSM")?>",
    success: function(response){
      document.getElementById("data_dari").innerHTML = response[0].dari;
      document.getElementById("data_id_surat").innerHTML = response[0].id_surat_masuk;
      document.getElementById("data_no_surat").innerHTML = response[0].no_surat_masuk;
      document.getElementById("data_tanggal").innerHTML = response[0].tanggal_surat_masuk;
      document.getElementById("data_keterangan").innerHTML = response[0].keterangan_surat_masuk;
    }
  });
}

function openEdit(id){
  $('#edit_data_surat').modal('show');
  $.ajax({
    data: {'id_surat':id},
    dataType:"json",
    type: "post",
    url: "<?php echo site_url("/SuratMasuk/ambilDetailSM")?>",
    success: function(response){
      document.getElementById("edit_surat_dari").value = response[0].dari;
      document.getElementById("edit_id_surat").value = response[0].id_surat_masuk;
      document.getElementById("edit_no_surat").value = response[0].no_surat_masuk;
      document.getElementById("edit_tanggal_surat").value = response[0].tanggal_surat_masuk;
      document.getElementById("edit_keterangan").value = response[0].keterangan_surat_masuk;
    }
  });
}

function simpanEditMasuk(){
  var data = $("#formEditSuratMasuk").serialize();
  $.ajax({
    data: data,
    type: "post",
    dataType:"json",
    url: "<?php echo site_url("/SuratMasuk/simpanEditMasuk")?>",
    success: function(response){
      if(response.status=="gagal"){
        swal({
              title: "Gagal",
              text: "Gagal Mengupdate Surat Masuk",
              type: "error"
        });
      }
      else  {
          swal({
            title: "Berhasil",
            text: "Data Surat Masuk Berhasil Di Ubah",
            type: "success"
          });
          $('#surat').DataTable().ajax.reload();
          $('#edit_data_surat').modal('hide');

      }
    }
  });

 }
 $('#input_tanggal input').datepicker({
      format: "yyyy-mm-dd",
      language: "id"
    });
</script>
