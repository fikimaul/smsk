<!DOCTYPE html>
<html lang="en">
<head>

      <link href="<?php echo base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- MetisMenu CSS -->
      <link href="<?php echo base_url()?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

      <!-- Custom CSS -->
      <link href="<?php echo base_url()?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

      <!-- Morris Charts CSS -->
      <link href="<?php echo base_url()?>assets/vendor/morrisjs/morris.css" rel="stylesheet">

      <!-- Custom Fonts -->
      <link href="<?php echo base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="<?php echo base_url()?>assets/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?php echo base_url()?>assets/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link href="<?php echo base_url()?>assets/vendor/datepicker/css/bootstrap-datepicker.standalone.css" rel="stylesheet">
  <!--sweetalert-->
  <script src="<?php echo base_url()?>assets/vendor/sweetalert/sweetalert.min.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/sweetalert/sweetalert.css">
  <!--mycss-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/vendor/mycss/mycss.css">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistem Informasi Surat Menyurat</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url()?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url()?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
       <script src="<?php echo base_url()?>assets/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url()?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url()?>assets/vendor/metisMenu/metisMenu.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="<?php echo base_url()?>assets/vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url()?>assets/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets/vendor/datatables-responsive/dataTables.responsive.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url()?>assets/dist/js/sb-admin-2.js"></script>

        <script src="<?php echo base_url()?>assets/vendor/datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url()?>assets/vendor/datepicker/locales/bootstrap-datepicker.id.min.js"></script>
        <script src="<?php echo base_url()?>assets/vendor/md5-js/md5.js"></script>
        <!-- JQUERY VALIDATION -->
        <script src="<?php echo base_url()?>assets/vendor/jquery-validation/jquery.validate.js"></script>
        <script src="<?php echo base_url()?>assets/vendor/jquery-validation/additional-methods.js"></script>
        <script src="<?php echo base_url()?>assets/vendor/jquery-validation/localization/messages_id.js"></script>
    <script>
        function logout(){
        var href = document.getElementById("logout").href;
        swal({
          title: "Are you sure?",
          text: "You will logout",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
            function(isConfirm){
              if (isConfirm) {
                window.location = href;
                return true;
              } else {
                swal("Cancelled", "Your action is cancelled ", "error");
              }
            });
            return false;
        }
    </script>
</head>

<body>

    <div id="wrapper">

      <!-- Navigation -->
      <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
          <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php" >Sistem Informasi Surat Menyurat</a>
          </div>
          <!-- /.navbar-header -->

          <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a ><i class="fa fa-user fa-fw"> &nbsp;
                          <?php if (isset($nama_admin)){ echo $nama_admin; }?>
                        </i></a>
                        </li>
                        <li class="divider"></li>
                        <li class="btn btn-danger btn-sm"><a href="<?php echo site_url('login/logout'); ?>" onclick="return logout()" id="logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
              <!-- /.dropdown -->
          </ul>
          <!-- /.navbar-top-links -->

          <div class="navbar-default sidebar" role="navigation">
              <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                      <li>
                          <a href="#HOME" onclick="BukaHome()"><i class="fa fa-dashboard fa-fw"></i> Home</a>
                      </li>
                      <li>
                          <a href="#SM" onclick="BukaSuratMasuk()"><i class="glyphicon glyphicon-import"></i> Surat Masuk</a>
                      </li>
                      <li>
                          <a href="#SK" onclick="BukaSuratKeluar()"><i class="glyphicon glyphicon-export"></i> Surat Keluar</a>
                      </li>
                  </ul>
              </div>
              <!-- /.sidebar-collapse -->
          </div>
          <!-- /.navbar-static-side -->
      </nav>
      <div id="page-wrapper">
