<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
   public function __construct(){
      parent::__construct();
      $this->load->model('ModelUser');
   }
   
   public function index(){
      if ($this->session->userdata('nama_admin')!=null) {
            redirect(base_url());
         }
         $this->load->view('login/login.php');
      }

   public function ProsesLogin(){
      if($_POST['username']!=null){
         $where['username'] = $this->input->post('username');
         $where['password'] = md5($this->input->post('password'));

         $hasil = $this->ModelUser->getUserById($where);
   
         if(count($hasil)==1){
         foreach ($hasil as $value) {
            $sess_data['id'] = $value->id;
            $sess_data['username'] = $value->username;
            $sess_data['nama_admin'] = $value->nama;
            $this->session->set_userdata($sess_data);
            echo '{"status":"berhasil"}';
         }
         }else{
         echo '{"status":"gagal"}';
         }
      }else{
         echo '{"status":"gagal"}';
      }
   }
   public function logout(){
      $this->session->unset_userdata('id');
      $this->session->unset_userdata('username');
      $this->session->unset_userdata('nama_admin');
      session_destroy();
      redirect('login');
   }
}
