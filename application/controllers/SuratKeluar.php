<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratKeluar extends CI_Controller {
   public function __construct(){
      parent::__construct();
      $this->load->model('ModelSuratKeluar');
   }
   
   public function index(){
      $this->load->view('surat_keluar/surat_keluar');
   }

   public function ambilData(){
      $data = json_encode($this->ModelSuratKeluar->getSuratKeluar());
      $res = '{
         "data":
         '.$data.'
      }';
      print_r($res);
   }

   public function simpanSuratKeluar(){
      if($_POST['no_surat']!=null){
         $data['no_surat_keluar'] = $this->input->post('no_surat');
         $data['kepada'] = $this->input->post('surat_kepada');
         $data['tanggal_surat_keluar'] = $this->input->post('tanggal_surat');
         $data['keterangan_surat_keluar'] =$this->input->post('keterangan');
   
         $this->ModelSuratKeluar->simpanSuratKeluar($data);
         echo '{"status":"berhasil"}';
      }else{
         echo '{"status":"gagal"}';
      }
   }

   public function hapusSuratKeluar(){
      $data['id_surat_keluar'] = $this->input->post('id_surat');

      $this->ModelSuratKeluar->hapusSuratKeluar($data);
   }

   public function ambilDetailSK(){
      $data['id_surat_keluar'] = $this->input->post('id_surat');

      $data_res = $this->ModelSuratKeluar->getSuratKeluarDetail($data);
      $send_rest = json_encode($data_res);
      print_r($send_rest);
   }

   public function simpanEditKeluar(){
      if($_POST['edit_no_surat']!=null){
         $where['id_surat_keluar'] = $this->input->post('edit_id_surat');

         $data['no_surat_keluar'] = $this->input->post('edit_no_surat');
         $data['kepada'] = $this->input->post('edit_surat_dari');
         $data['tanggal_surat_keluar'] = $this->input->post('edit_tanggal_surat');
         $data['keterangan_surat_keluar'] =$this->input->post('edit_keterangan');

         $this->ModelSuratKeluar->updateSuratKeluar($data,$where);
         echo '{"status":"berhasil"}';
      }
      else{
         echo '{"status":"gagal"}';
      }
   }
}
