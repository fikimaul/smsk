<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SuratMasuk extends CI_Controller {
   public function __construct(){
      parent::__construct();
      $this->load->model('ModelSuratMasuk');
   }
   
   public function index(){
      $this->load->view('surat_masuk/surat_masuk');
   }

   public function ambilData(){
      $data = json_encode($this->ModelSuratMasuk->getSuratMasuk());
      $res = '{
         "data":
         '.$data.'
      }';
      print_r($res);
   }

   public function simpanSuratMasuk(){
      if($_POST['no_surat']!=null){
         $data['no_surat_masuk'] = $this->input->post('no_surat');
         $data['dari'] = $this->input->post('surat_dari');
         $data['tanggal_surat_masuk'] = $this->input->post('tanggal_surat');
         $data['keterangan_surat_masuk'] =$this->input->post('keterangan');

         $this->ModelSuratMasuk->simpanSuratMasuk($data);
            echo '{"status":"berhasil"}';
      }else{
         echo '{"status":"gagal"}';
      }
   }

   public function hapusSuratMasuk(){
      $data['id_surat_masuk'] = $this->input->post('id_surat');

      $this->ModelSuratMasuk->hapusSuratMasuk($data);
   }

   public function ambilDetailSM(){
      $data['id_surat_masuk'] = $this->input->post('id_surat');

      $data_res = $this->ModelSuratMasuk->getSuratMasukDetail($data);
      $send_rest = json_encode($data_res);
      print_r($send_rest);
   }

   public function simpanEditMasuk(){
      if($_POST['edit_no_surat']!=null){
         $where['id_surat_masuk'] = $this->input->post('edit_id_surat');

         $data['no_surat_masuk'] = $this->input->post('edit_no_surat');
         $data['dari'] = $this->input->post('edit_surat_dari');
         $data['tanggal_surat_masuk'] = $this->input->post('edit_tanggal_surat');
         $data['keterangan_surat_masuk'] =$this->input->post('edit_keterangan');

         $this->ModelSuratMasuk->updateSuratMasuk($data,$where);
         echo '{"status":"berhasil"}';
      }
      else{
         echo '{"status":"gagal"}';
      }
   }
}
