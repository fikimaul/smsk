<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('nama_admin')=="") {
			redirect('login');
		}
	}
	
	public function index()
	{
		$data['nama_admin'] = $this->session->userdata('nama_admin');
		$this->load->view('templates/atas',$data);
		$this->load->view('templates/bawah');
		$this->load->view('dashboard/load_page');
	}

	public function Home(){
		$this->load->view('dashboard/home');
	}
}
