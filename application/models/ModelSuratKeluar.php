<?php

class ModelSuratKeluar extends CI_Model {
   public function getSuratKeluar(){
      $query = $this->db->get("surat_keluar");
      return $query->result();
   }

   public function simpanSuratKeluar($data){
      $this->db->insert("surat_keluar",$data);
   }

   public function hapusSuratKeluar($where){
      $this->db->delete("surat_keluar",$where);
   }
   
   public function getSuratKeluarDetail($where){  
      $query = $this->db->get_where("surat_keluar",$where);
      return $query->result();
   }

   public function updateSuratKeluar($data,$where){
      $this->db->update('surat_keluar', $data,$where);
   }
}
?>
