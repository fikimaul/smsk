<?php

class ModelSuratMasuk extends CI_Model {
   public function getSuratMasuk(){
      $query = $this->db->get("surat_masuk");
      return $query->result();
   }

   public function simpanSuratMasuk($data){
      $this->db->insert("surat_masuk",$data);
   }

   public function hapusSuratMasuk($where){
      $this->db->delete("surat_masuk",$where);
   }

   public function getSuratMasukDetail($where){
      $query = $this->db->get_where("surat_masuk",$where);
      return $query->result();
   }

   public function updateSuratMasuk($data,$where){
      $this->db->update('surat_masuk', $data,$where);
   }
}
?>
